/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "device_networking_collect.h"

#include "sam_log.h"
#include "sa_profiles.h"
#include "system_ability_manager.h"

using namespace std;

using namespace OHOS::DistributedHardware;

namespace OHOS {
namespace {
const std::string PKG_NAME = "Samgr_Networking";
const std::string SA_TAG_DEVICE_ON_LINE = "deviceonline";
constexpr uint32_t INIT_EVENT = 10;
constexpr uint32_t DM_DIED_EVENT = 11;
constexpr int64_t DELAY_TIME = 1000;
constexpr int32_t DISTRIBUTED_HARDWARE_DEVICEMANAGER_SA_ID = 4802;
}
DeviceNetworkingCollect::DeviceNetworkingCollect(const sptr<IReport>& report)
    : ICollectPlugin(report)
{
}

int32_t DeviceNetworkingCollect::OnStart()
{
    HILOGI("DeviceNetworkingCollect OnStart called");
    workHandler_ = std::make_shared<WorkHandler>(this);
    initCallback_ = std::make_shared<DeviceInitCallBack>(workHandler_);
    stateCallback_ = std::make_shared<DeviceStateCallback>(this);
    workHandler_->SendEvent(INIT_EVENT);
    return ERR_OK;
}

int32_t DeviceNetworkingCollect::OnStop()
{
    DeviceManager::GetInstance().UnRegisterDevStateCallback(PKG_NAME);
    if (workHandler_ != nullptr) {
        workHandler_ = nullptr;
    }
    initCallback_ = nullptr;
    ClearDeviceOnlineSet();
    stateCallback_ = nullptr;
    return ERR_OK;
}

bool DeviceNetworkingCollect::IsDmReady()
{
    auto dmProxy = SystemAbilityManager::GetInstance()->CheckSystemAbility(
        DISTRIBUTED_HARDWARE_DEVICEMANAGER_SA_ID);
    if (dmProxy != nullptr) {
        IPCObjectProxy* proxy = reinterpret_cast<IPCObjectProxy*>(dmProxy.GetRefPtr());
        // make sure the proxy is not dead
        if (proxy != nullptr && !proxy->IsObjectDead()) {
            return true;
        }
    }
    return false;
}

bool DeviceNetworkingCollect::ReportMissedEvents()
{
    std::vector<DmDeviceInfo> devList;
    int32_t ret = DeviceManager::GetInstance().GetTrustedDeviceList(PKG_NAME, "", devList);
    if (ret != ERR_OK) {
        HILOGE("DeviceNetworkingCollect GetTrustedDeviceList error");
        return false;
    }
    bool isPreviousOnline = IsOnline();
    if (isPreviousOnline) {
        ClearDeviceOnlineSet();
        if (devList.empty()) {
            // send offline msg
            OnDemandEvent event = { DEVICE_ONLINE, SA_TAG_DEVICE_ON_LINE, "off" };
            ReportEvent(event);
        } else {
            // update the online set;
            for (DmDeviceInfo& devInfo : devList) {
                UpdateDeviceOnlineSet(devInfo.networkId);
            }
        }
    } else {
        // offline --> online
        if (!devList.empty()) {
            // update the online set;
            for (DmDeviceInfo& devInfo : devList) {
                UpdateDeviceOnlineSet(devInfo.networkId);
            }
            // send online msg
            OnDemandEvent event = { DEVICE_ONLINE, SA_TAG_DEVICE_ON_LINE, "on" };
            ReportEvent(event);
        }
    }
    return true;
}

bool DeviceNetworkingCollect::AddDeviceChangeListener()
{
    HILOGI("DeviceNetworkingCollect AddDeviceChangeListener called");
    if (IsDmReady()) {
        int32_t ret = DeviceManager::GetInstance().InitDeviceManager(PKG_NAME, initCallback_);
        if (ret != ERR_OK) {
            HILOGE("DeviceNetworkingCollect InitDeviceManager error");
            return false;
        }
        if (!ReportMissedEvents()) {
            HILOGE("DeviceNetworkingCollect ReportMissedEvents error");
            return false;
        }
        ret = DeviceManager::GetInstance().RegisterDevStateCallback(PKG_NAME, "", stateCallback_);
        if (ret != ERR_OK) {
            DeviceManager::GetInstance().UnRegisterDevStateCallback(PKG_NAME);
            HILOGE("DeviceNetworkingCollect RegisterDevStateCallback error");
            return false;
        }
        return true;
    }
    return false;
}

void DeviceNetworkingCollect::UpdateDeviceOnlineSet(const std::string& deviceId)
{
    if (stateCallback_ != nullptr) {
        stateCallback_->UpdateDeviceOnlineSet(deviceId);
    }
}

void DeviceNetworkingCollect::ClearDeviceOnlineSet()
{
    if (stateCallback_ != nullptr) {
        stateCallback_->ClearDeviceOnlineSet();
    }
}

bool DeviceNetworkingCollect::CheckCondition(const OnDemandCondition& condition)
{
    bool isOnline = IsOnline();
    if (condition.value == "on" && isOnline) {
        return true;
    }
    if (condition.value == "off" && !isOnline) {
        return true;
    }
    return false;
}

bool DeviceNetworkingCollect::IsOnline()
{
    if (stateCallback_ != nullptr) {
        return stateCallback_->IsOnline();
    }
    return false;
}

void DeviceInitCallBack::OnRemoteDied()
{
    HILOGI("DeviceNetworkingCollect DeviceInitCallBack OnRemoteDied");
    if (handler_ != nullptr) {
        handler_->SendEvent(DM_DIED_EVENT, DELAY_TIME);
    }
}

void DeviceStateCallback::OnDeviceOnline(const DmDeviceInfo& deviceInfo)
{
    HILOGI("DeviceNetworkingCollect DeviceStateCallback OnDeviceOnline");
    bool isOnline = false;
    {
        lock_guard<mutex> autoLock(deviceOnlineLock_);
        isOnline = deviceOnlineSet_.empty();
        deviceOnlineSet_.emplace(deviceInfo.networkId);
    }
    if (isOnline) {
        OnDemandEvent event = { DEVICE_ONLINE, SA_TAG_DEVICE_ON_LINE, "on" };
        if (collect_ != nullptr) {
            collect_->ReportEvent(event);
        }
    }
}

void DeviceStateCallback::OnDeviceOffline(const DmDeviceInfo& deviceInfo)
{
    HILOGI("DeviceNetworkingCollect DeviceStateCallback OnDeviceOffline");
    bool isOffline = false;
    {
        lock_guard<mutex> autoLock(deviceOnlineLock_);
        deviceOnlineSet_.erase(deviceInfo.networkId);
        isOffline = deviceOnlineSet_.empty();
        if (isOffline) {
            isExistDeviceReady_ = false;
        }
    }
    if (isOffline) {
        OnDemandEvent event = { DEVICE_ONLINE, SA_TAG_DEVICE_ON_LINE, "off" };
        if (collect_ != nullptr) {
            collect_->ReportEvent(event);
        }
    }
}

void DeviceStateCallback::ClearDeviceOnlineSet()
{
    lock_guard<mutex> autoLock(deviceOnlineLock_);
    deviceOnlineSet_.clear();
}

bool DeviceStateCallback::IsOnline()
{
    lock_guard<mutex> autoLock(deviceOnlineLock_);
    return !deviceOnlineSet_.empty();
}

void DeviceStateCallback::UpdateDeviceOnlineSet(const std::string& deviceId)
{
    lock_guard<mutex> autoLock(deviceOnlineLock_);
    deviceOnlineSet_.emplace(deviceId);
}

void DeviceStateCallback::OnDeviceChanged(const DmDeviceInfo& deviceInfo)
{
    HILOGD("DeviceNetworkingCollect OnDeviceChanged called");
}

void DeviceStateCallback::OnDeviceReady(const DmDeviceInfo& deviceInfo)
{
    HILOGI("DeviceNetworkingCollect DeviceStateCallback OnDeviceReady");
    lock_guard<mutex> autoLock(deviceOnlineLock_);
    if (!isExistDeviceReady_) {
        OnDemandEvent event = { DEVICE_ONLINE, SA_TAG_DEVICE_ON_LINE, "ready" };
        if (collect_ != nullptr) {
            collect_->ReportEvent(event);
        }
        isExistDeviceReady_ = true;
    }
}

void WorkHandler::ProcessEvent(uint32_t eventId)
{
    if (collect_ == nullptr) {
        HILOGE("DeviceNetworkingCollect ProcessEvent collect or event is null!");
        return;
    }
    if (eventId != INIT_EVENT && eventId != DM_DIED_EVENT) {
        HILOGE("DeviceNetworkingCollect ProcessEvent error event code!");
        return;
    }
    if (handler_ == nullptr) {
        HILOGE("DeviceNetworkingCollect SendEvent handler is null!");
        return;
    }
    if (!collect_->AddDeviceChangeListener()) {
        HILOGW("DeviceNetworkingCollect AddDeviceChangeListener retry");
        auto task = std::bind(&WorkHandler::ProcessEvent, this, INIT_EVENT);
        if (handler_ == nullptr) {
            HILOGE("DeviceNetworkingCollect ProcessEvent handler is null!");
            return;
        }
        handler_->PostTask(task, DELAY_TIME);
    }
    HILOGI("DeviceNetworkingCollect AddDeviceChangeListener success");
}

bool WorkHandler::SendEvent(uint32_t eventId)
{
    if (handler_ == nullptr) {
        HILOGE("DeviceNetworkingCollect SendEvent handler is null!");
        return false;
    }
    auto task = std::bind(&WorkHandler::ProcessEvent, this, eventId);
    return handler_->PostTask(task);
}

bool WorkHandler::SendEvent(uint32_t eventId, uint64_t delayTime)
{
    if (handler_ == nullptr) {
        HILOGE("DeviceNetworkingCollect SendEvent handler is null!");
        return false;
    }
    auto task = std::bind(&WorkHandler::ProcessEvent, this, eventId);
    return handler_->PostTask(task, delayTime);
}
}  // namespace OHOS
